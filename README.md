### About
This addon adds the possibility to send Blender objects through the system shared memory, using the [shmdata](https://gitlab.com/sat-mtl/tools/shmdata) library. Currently, only meshes are supported.

### License
This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version (http://www.gnu.org/licenses/).

### Authors
* Emmanuel Durand

### Project URL
This project can be found on the [SAT Metalab repository](https://gitlab.com/sat-mtl/tools/blender-addon-shmdata)

# Requirements
This addon needs [shmdata](https://gitlab.com/sat-mtl/tools/shmdata) to be installed and compiled against the same Python version as the one Blender embeds.

It is also strongly advised to have the `pyopengl` Python module installed to get better performances. It can be downloaded from [Pypi](https://pypi.org/project/PyOpenGL/) or using `pip`:

```bash
pip3 install pyopengl
```

In any case this has to be done using the Python interpreter which is used by Blender. This depends whether Blender is using the system interpreter (in which case the previous command can be ran from anywhere) or if it is using its own version of the interpreter. In the latter case you will have to find the location of the interpreter and the associated Python modules and install the `pyopengl` module directly from the compressed archive, downloaded on the website.

### Installation
This addon can be installed in two ways:
* if you are a developer, you can install a symbolic link to your Blender addon directory with the _install.sh_ script.
* if you are a user, you can "build" the addon with the _build.sh_ script, then install the resulting _blender-shmdata.zip_ file like any other Blender addon
