# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import bpy


class ShmdataMeshPropertiesPanel(bpy.types.Panel):
    bl_space_type = "PROPERTIES"
    bl_region_type = "WINDOW"
    bl_context = "object"
    bl_label = "Shmdata export"
    bl_idname = "SHMDATA_PT_mesh_properties_panel"

    bpy.types.Object.shmdata_mesh_active = bpy.props.BoolProperty(
        name="Export to Shmdata",
        description="Export the mesh to shared memory through shmdata",
        default=False
    )

    @classmethod
    def poll(cls, context) -> bool:
        obj = context.active_object
        return obj and obj.type == 'MESH'

    def draw(self, context) -> None:
        layout = self.layout
        main_col = layout.column(align=True)
        row = main_col.row(align=True)
        row.operator("shmdata.send_mesh_to_shmdata", text="Send mesh" if not context.active_object.shmdata_mesh_active else "Stop sending mesh")


class ShmdataCameraSettings(bpy.types.PropertyGroup):
    enable : bpy.props.BoolProperty(
        name="Export to Shmdata",
        description="Send the render for this camera to shared memory through shmdata",
        default=False
    )

    width : bpy.props.IntProperty(
        name="Capture width",
        default=720,
        description="Capture width in pixels",
        min=360,
        max=2048
    )

    height : bpy.props.IntProperty(
        name="Capture height",
        default=480,
        description="Capture height in pixels",
        min=240,
        max=2048
    )

    workspace : bpy.props.StringProperty(
        name="workspace",
        default="Layout",
        description="Workspace from which to use the Overlay and Shading properties"
    )

    scene : bpy.props.StringProperty(
        name="scene",
        default="Scene",
        description="Scene to render"
    )

    layer : bpy.props.StringProperty(
        name="layer",
        default="ViewLayer",
        description="Layer in Scene to render"
    )


class ShmdataCameraPropertiesPanel(bpy.types.Panel):
    bl_space_type = "PROPERTIES"
    bl_region_type = "WINDOW"
    bl_context = "data"
    bl_label = "Shmdata export"
    bl_idname = "SHMDATA_PT_camera_properties_panel"
    COMPAT_ENGINES = {"BLENDER_RENDER", "BLENDER_EEVEE", "BLENDER_WORKBENCH"}

    @classmethod
    def poll(cls, context) -> bool:
        obj = context.active_object
        is_camera = obj and obj.type == 'CAMERA'
        is_engine_supported = context.engine in cls.COMPAT_ENGINES
        return is_camera and is_engine_supported

    def draw(self, context) -> None:
        camera = context.camera

        layout = self.layout
        main_col = layout.column(align=True)
        row = main_col.row(align=True)
        row.operator("shmdata.send_camera_to_shmdata", text="Send camera" if not camera.shmdata.enable else "Stop sending camera")

        row = main_col.row(align=True)
        row.prop(camera.shmdata, "width", slider=True)
        row.prop(camera.shmdata, "height", slider=True)

        row = layout.row(align=True)
        row.prop_search(camera.shmdata, 'workspace', bpy.data, 'workspaces', text='Shading')

        col = layout.column()

        sub = col.column(align=True)
        sub.prop_search(camera.shmdata, 'scene', bpy.data, 'scenes', text='Scene')
        sub.prop_search(camera.shmdata, 'layer', bpy.data.scenes[camera.shmdata.scene], 'view_layers', text='Layer')
