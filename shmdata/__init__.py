
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

if "bpy" in locals():
    import imp
    imp.reload('operators')
    imp.reload('properties_panel')
else:
    import bpy
    from .operators_mesh import ShmdataMeshExport
    from .operators_camera import ShmdataCameraExport
    from .properties_panel import ShmdataCameraSettings, ShmdataCameraPropertiesPanel, ShmdataMeshPropertiesPanel

__copyright__ = """
This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public
License as published by the Free Software Foundation; either version 3 of the license, or (at your option) any later
version (http://www.gnu.org/licenses/).
"""
__license__ = "GPLv3"

bl_info = {
    "name": "Shmdata",
    "author": "Emmanuel Durand",
    "version": (1, 0, 0),
    "blender": (2, 80, 0),
    "description": "Allows for sending meshes through shared memory",
    "category": "Import-Export"
}

classes = (
    ShmdataCameraSettings,
    ShmdataCameraExport,
    ShmdataCameraPropertiesPanel,

    ShmdataMeshExport,
    ShmdataMeshPropertiesPanel
)


def register() -> None:
    """
    Register OpenPose addon classes
    """
    for cls in classes:
        bpy.utils.register_class(cls)

    bpy.types.Camera.shmdata = bpy.props.PointerProperty(type=ShmdataCameraSettings)


def unregister() -> None:
    """
    Unregister OpenPose addon classes
    """
    for cls in classes:
        bpy.utils.unregister_class(cls)


if __name__ == "__main__":
    register()
