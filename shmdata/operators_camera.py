#
# Copyright (C) 2015 Emmanuel Durand
#
# This file is part of Splash (http://github.com/paperManu/splash)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Splash is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Splash.  If not, see <http://www.gnu.org/licenses/>.
#

import bpy
import gpu
import time
import sys

from bpy.types import Operator
from dataclasses import dataclass
from typing import Any, Dict, Optional

sys.path.insert(0, '/usr/local/lib/python3/dist-packages')

try:
    from pyshmdata import Writer
except ModuleNotFoundError as ex:
    print(f"Could not load pyshmdata: {ex}")

try:
    from OpenGL.GL import glBindTexture, glGetTexImage, glGetIntegerv
    from OpenGL.GL import GL_TEXTURE_2D, GL_TEXTURE_BINDING_2D, GL_RGBA, GL_UNSIGNED_BYTE
    USE_BGL = False
except ModuleNotFoundError as ex:
    print(f"Could not load OpenGL.GL: {ex}. Falling back to bgl")
    from bgl import Buffer, glBindTexture, glGetTexImage
    from bgl import GL_TEXTURE_2D, GL_TEXTURE_BINDING_2D, GL_RGBA, GL_BYTE, GL_UNSIGNED_BYTE
    USE_BGL = True


class ShmdataCamera:
    targets: Dict[str, 'ShmdataCamera.Target'] = {}

    @dataclass
    class Target:
        object_name: str
        draw_handle: Any
        writer: Optional[Writer] = None
        start_time: float = 0.0

    @classmethod
    def add_target(cls, target: Target) -> None:
        if target.object_name not in ShmdataCamera.targets:
            ShmdataCamera.targets[target.object_name] = target

    @classmethod
    def get_target(cls, object_name: str) -> Optional['ShmdataCamera.Target']:
        if object_name not in ShmdataCamera.targets.keys():
            return None

        return ShmdataCamera.targets[object_name]

    @classmethod
    def remove_target(cls, object_name: str) -> None:
        if object_name not in ShmdataCamera.targets.keys():
            return

        target = ShmdataCamera.targets[object_name]
        ShmdataCamera.targets.pop(target.object_name)

    @classmethod
    def capture_camera_callback(
        cls,
        camera: bpy.types.Camera,
        context: bpy.types.Context,
        space: bpy.types.Space,
        region: bpy.types.Region,
        scene: bpy.types.Scene,
        layer: bpy.types.LayerObjects,
        offscreen: gpu.types.GPUOffScreen,
        writer: Writer
    ) -> None:
        width = offscreen.width
        height = offscreen.height

        apply_color_management = True

        view_matrix = camera.matrix_world.inverted()
        projection_matrix = camera.calc_matrix_camera(context.evaluated_depsgraph_get(), x=width, y=height)

        offscreen.draw_view3d(
            scene,
            layer,
            space,
            context.region,
            view_matrix,
            projection_matrix,
            do_color_management=apply_color_management
        )

        if USE_BGL:
            glBindTexture(GL_TEXTURE_2D, offscreen.color_texture)
            texture = Buffer(GL_BYTE, width * height * 4)
            glGetTexImage(GL_TEXTURE_2D, 0, GL_RGBA, GL_UNSIGNED_BYTE, texture)
        else:
            current_active_texture = glGetIntegerv(GL_TEXTURE_BINDING_2D)
            glBindTexture(GL_TEXTURE_2D, offscreen.color_texture)
            texture = glGetTexImage(GL_TEXTURE_2D, 0, GL_RGBA, GL_UNSIGNED_BYTE)
            glBindTexture(GL_TEXTURE_2D, current_active_texture)

        writer.push(bytearray(texture))


class ShmdataCameraExport(Operator):
    """
    Activate the output of the selected camera to shmdata
    """
    bl_idname = "shmdata.send_camera_to_shmdata"
    bl_label = "Activate the output of the selected camera to shmdata"

    def execute(self, context: bpy.types.Context):
        path_prefix = "/tmp/blender_shmdata_camera_"
        object_name = context.active_object.name
        path = path_prefix + context.active_object.name

        obj = context.active_object
        camera = context.camera

        if camera.shmdata.enable:
            camera.shmdata.enable = False
            target = ShmdataCamera.get_target(object_name)
            bpy.types.SpaceView3D.draw_handler_remove(target.draw_handle, 'WINDOW')
            ShmdataCamera.remove_target(object_name)
        else:
            camera.shmdata.enable = True

            width = camera.shmdata.width
            height = camera.shmdata.height

            writer = Writer(path=path, datatype=f"video/x-raw, format=(string)RGBA, width=(int){width}, height=(int){height}")

            offscreen = gpu.types.GPUOffScreen(width, height)
            space = context.space_data
            region = context.region

            for area in bpy.data.workspaces[camera.shmdata.workspace].screens[0].areas:
                if area.type == 'VIEW_3D':
                    region = area.regions[0]
                    for space in area.spaces:
                        if space.type == 'VIEW_3D':
                            space = space

            scene = bpy.data.scenes[camera.shmdata.scene]
            layer = scene.view_layers[camera.shmdata.layer]

            current_scene = bpy.context.window.scene
            current_layer = bpy.context.window.view_layer

            # Open the rendered layer and scene to avoid a crash of Blender,
            # then revert to the current scene and layer
            bpy.context.window.scene = scene
            bpy.context.window.view_layer = layer
            bpy.context.window.scene = current_scene
            bpy.context.window.view_layer = current_layer

            args = (bpy.types.Camera(obj), context, space, region, scene, layer, offscreen, writer)
            draw_handle = bpy.types.SpaceView3D.draw_handler_add(ShmdataCamera.capture_camera_callback, args, 'WINDOW', 'POST_PIXEL')

            target = ShmdataCamera.Target(object_name=object_name,
                                          writer=writer,
                                          draw_handle=draw_handle,
                                          start_time=time.clock_gettime(time.CLOCK_REALTIME))

            ShmdataCamera.add_target(target)

        return {'FINISHED'}
